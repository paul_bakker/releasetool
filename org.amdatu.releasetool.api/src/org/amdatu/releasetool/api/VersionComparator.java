package org.amdatu.releasetool.api;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VersionComparator implements Comparator<String> {
	@Override
	public int compare(String version1, String version2) {
		Pattern pattern = Pattern.compile("^([0-9]+)\\.([0-9]+)\\.([0-9]+)");
		Matcher m1 = pattern.matcher(version1);
		Matcher m2 = pattern.matcher(version2);
		m1.find();
		m2.find();
		Integer major1 = Integer.parseInt(m1.group(1));
		Integer major2 = Integer.parseInt(m2.group(1));
		Integer minor1 = Integer.parseInt(m1.group(2));
		Integer minor2 = Integer.parseInt(m2.group(2));
		Integer fix1 = Integer.parseInt(m1.group(3));
		Integer fix2 = Integer.parseInt(m2.group(3));

		int cMajor = major1.compareTo(major2);
		if (cMajor == -1) {
			return -1;
		} else if (cMajor == 1) {
			return 1;
		} else {
			int cMinor = minor1.compareTo(minor2);
			if (cMinor == -1) {
				return -1;
			} else if (cMinor == 1) {
				return 1;
			} else {
				return fix1.compareTo(fix2);
			}
		}

	}
}
