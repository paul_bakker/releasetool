package org.amdatu.releasetool.repositories.extender;

import java.util.Dictionary;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.releasetool.api.Repository;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
    private volatile DependencyManager m_dependencyManager;
    private Map<Long, Component> m_components = new ConcurrentHashMap<Long, Component>();
    
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent()
            .setImplementation(this)
            .add(createServiceDependency().setService(LogService.class).setRequired(false))
            .add(createBundleDependency()
                .setStateMask(Bundle.ACTIVE)
                .setFilter("(Repository=*)")
                .setCallbacks("add", "remove")
            )
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }

    public void add(Bundle bundle) throws Exception {
        Dictionary<?, ?> headers = bundle.getHeaders();
        String repositoryClass = (String) headers.get("Repository");
        String pid = (String) headers.get("PID");

        @SuppressWarnings("unchecked")
        Class<Repository> componentClass =
            bundle.loadClass(repositoryClass);

        Properties properties = new Properties();
        properties.put(Constants.SERVICE_PID, pid);
        RepositoryServiceFactory factory = new RepositoryServiceFactory(m_dependencyManager, componentClass);

        Component component = m_dependencyManager.createComponent()
            .setInterface(ManagedServiceFactory.class.getName(), properties)
            .setImplementation(factory);
        m_dependencyManager.add(component);
        
        m_components.put(bundle.getBundleId(), component);

    }

    public void remove(Bundle bundle) {
        Component component = m_components.get(bundle.getBundleId());
        m_dependencyManager.remove(component);
        m_components.remove(bundle.getBundleId());
    }
}
