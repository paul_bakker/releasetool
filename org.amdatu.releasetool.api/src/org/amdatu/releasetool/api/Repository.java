package org.amdatu.releasetool.api;

import java.util.List;

public interface Repository {
    void setRepositoryId(String id);

    String getRepositoryId();

    void setUrl(String url);

    String getUrl();

    List<Artifact> listArtifacts();
    
    void clearCache();
}
