package org.amdatu.releasetool.repositories.extender;

import java.util.Dictionary;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.releasetool.api.Repository;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

public class RepositoryServiceFactory implements ManagedServiceFactory {
    private volatile DependencyManager m_dependencyManager;
    private Map<String, Component> m_components = new ConcurrentHashMap<String, Component>();
    private final Class<Repository> serviceClass;

    public RepositoryServiceFactory(DependencyManager dependencyManager, Class<Repository> serviceClass) {
        m_dependencyManager = dependencyManager;
        this.serviceClass = serviceClass;
    }

    @Override
    public String getName() {
        return serviceClass.getName();
    }

    @Override
    public void updated(String pid, Dictionary properties) throws ConfigurationException {
        
        if (properties != null) {
            System.out.println("Updating config: " + properties);
            String id = (String) properties.get("repositoryId");
            String url = (String) properties.get("url");
            
            
            try {
                Repository repository = serviceClass.newInstance();
                repository.setRepositoryId(id);
                repository.setUrl(url);
                
                Component component =
                    m_dependencyManager.createComponent().setInterface(Repository.class.getName(), properties)
                        .setImplementation(repository);
                m_dependencyManager.add(component);
                m_components.put(pid, component);
            }
            catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void deleted(String pid) {
        System.out.println("Removing service " + pid);
        m_dependencyManager.remove(m_components.get(pid));
        m_components.remove(pid);
    }

}
