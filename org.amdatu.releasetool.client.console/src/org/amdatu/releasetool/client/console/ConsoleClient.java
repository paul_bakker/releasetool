package org.amdatu.releasetool.client.console;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.releasetool.api.Artifact;
import org.amdatu.releasetool.api.Repository;
import org.amdatu.releasetool.api.VersionComparator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

public class ConsoleClient {
    private Map<String, ServiceReference> m_repositories = new ConcurrentHashMap<String, ServiceReference>();
    private volatile ConfigurationAdmin m_configurationAdmin;
    private volatile BundleContext m_bundleContext;
    
    public void listRepos() {
        Set<String> keySet = m_repositories.keySet();
        for(String repoId : keySet) {
            System.out.println(repoId);
        }
    }
    
    public void addRepo(String factoryPid, String id, String url) throws Exception{
        Configuration config = m_configurationAdmin.createFactoryConfiguration(factoryPid, null);
        Properties properties = new Properties();
        properties.put("repositoryId", id);
        properties.put("url", url);
        config.update(properties);
    }
    
    public void removeRepo(String id) throws Exception{
        ServiceReference serviceReference = m_repositories.get(id);
        String pid = (String)serviceReference.getProperty("service.pid");
        Configuration config = m_configurationAdmin.getConfiguration(pid);
        config.delete();
    }
    
    public void listArtifacts(String repoId) {
        Repository repository = getRepository(repoId);
        System.out.println(repository);
        for(Artifact artifact : repository.listArtifacts()) {
            System.out.println(artifact);
        }
    }
    
    public void listLatestArtifacts(String repoId) {
        
        for(Artifact artifact : getLatestArtifacts(repoId)) {
            System.out.println(artifact);
        }
    }
    
    private Collection<Artifact> getLatestArtifacts(String repoId) {
        Repository repository = getRepository(repoId);
        Map<String, Artifact> artifacts = new HashMap<String, Artifact>(); 
        VersionComparator comparator = new VersionComparator();
        
        for(Artifact artifact : repository.listArtifacts()) {
            boolean containsArtifact = artifacts.containsKey(artifact.getName());
            if((containsArtifact && comparator.compare(artifact.getVersion(), artifacts.get(artifact.getName()).getVersion()) == 1) || !containsArtifact)  {
                artifacts.put(artifact.getName(), artifact);
            } 
        }
        
        return artifacts.values();
    }
    
    public void diffRepos(String repo1, String repo2) {
        Collection<Artifact> artifacts1 = getLatestArtifacts(repo1);
        Collection<Artifact> artifacts2 = getLatestArtifacts(repo2);
        
        for(Artifact artifact : artifacts1) {
            if(!artifacts2.contains(artifact)) {
                System.out.println(artifact);
            }
        }
    }

    public void clearRepo(String repoId) {
        getRepository(repoId).clearCache();
    }
    
    private Repository getRepository(String repoId) {
        ServiceReference serviceReference = m_repositories.get(repoId);
        return (Repository)m_bundleContext.getService(serviceReference);
    }
    
    
    public void repositoryAdded(ServiceReference serviceReference) {
        String id = getRepositoryId(serviceReference);
        m_repositories.put(id, serviceReference);
    }

    private String getRepositoryId(ServiceReference serviceReference) {
        String id = (String)serviceReference.getProperty("repositoryId");
        return id;
    }
    
    public void repositoryRemoved(ServiceReference serviceReference) {
        m_repositories.remove(getRepositoryId(serviceReference));
    }
}
