package org.amdatu.releasetool.client.console;

import java.util.Properties;

import org.amdatu.releasetool.api.Repository;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationAdmin;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties properties = new Properties();
        properties.put(CommandProcessor.COMMAND_SCOPE, "repositories");
        properties.put(CommandProcessor.COMMAND_FUNCTION, new String[] {"listRepos", "addRepo", "removeRepo", "listArtifacts", "diffRepos", "listLatestArtifacts", "clearRepo"});
        
        manager.add(manager.createComponent().setInterface(Object.class.getName(), properties).setImplementation(ConsoleClient.class)
            .add(createServiceDependency().setService(Repository.class).setCallbacks("repositoryAdded", "repositoryRemoved"))
            .add(createServiceDependency().setService(ConfigurationAdmin.class).setRequired(true)));
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }

}
