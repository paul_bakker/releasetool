package org.amdatu.releasetool.repositories.ace


import java.util.List;
import java.util.Collections.SynchronizedList;

import org.amdatu.releasetool.api.Artifact;
import org.amdatu.releasetool.api.Repository;
import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.Method.GET
import static groovyx.net.http.Method.POST
import static groovyx.net.http.ContentType.JSON

class AceRepository implements Repository {
    private String m_id;
    private String m_url;
    private List<Artifact> bundles = []

    @Override
    List<Artifact> listArtifacts() {
        if(bundles.size() > 0) {
            println "returning bundles from cache"
            return bundles
        }

        def http = new HTTPBuilder(m_url + "/client/work" )
        http.request(POST) {
            response.success = { resp ->
                getArtifacts(resp.headers.location)
            }
        }
        System.out.println("finish");

        return bundles
    }

    private void getArtifacts(location) {
        def http = new HTTPBuilder(location + '/artifact')
        http.request( GET, JSON ) {
            response.success = { resp, json ->

                json.each{  getArtifact(location + '/artifact/' + it) }
            }
        }
    }

    private void getArtifact(String url) {
        def http = new HTTPBuilder(url)
        http.request( GET, JSON ) {
            response.success = { resp, json ->
                if(json.attributes['Bundle-SymbolicName']) {
                    def artifact = new Artifact(json.attributes['Bundle-SymbolicName'], json.attributes['Bundle-Version'])
                    bundles << artifact
                }
            }
        }
    }

    @Override
    public void setRepositoryId(String id) {
        m_id = id;
    }

    @Override
    public String getRepositoryId() {
        return m_id;
    }

    @Override
    public void setUrl(String url) {
        m_url = url;
    }

    @Override
    public String getUrl() {
        return m_url;
    }

    @Override
    public void clearCache() {
        bundles.clear()
    }
}
