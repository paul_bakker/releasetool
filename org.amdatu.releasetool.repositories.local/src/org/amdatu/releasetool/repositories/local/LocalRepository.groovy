package org.amdatu.releasetool.repositories.local

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import org.amdatu.releasetool.api.Artifact;
import org.amdatu.releasetool.api.Repository;


class LocalRepository implements Repository {
    private String m_id;
    private String m_dir;

    @Override
    List<Artifact> listArtifacts() {
        File dir = new File(m_dir)
        if(!dir.exists()) {
            throw new RuntimeException("Directory $m_dir does not exists")
        }
        def artifacts = []
        scanForArtifacts(dir, artifacts)
        
        return artifacts
    }

    private void scanForArtifacts(File dir, List<Artifact> artifacts) {
        dir.eachFile {file ->
            if(file.isDirectory()) {
                scanForArtifacts(file, artifacts)
            } else {
                if(file.name.endsWith('.jar')) {
                    def manifest = readManifestFromJar(file)
                    artifacts << new Artifact( manifest.mainAttributes.getValue("Bundle-SymbolicName"),manifest.mainAttributes.getValue("Bundle-Version") )
                }
            }
        }
    }

    private Manifest readManifestFromJar(File jar) {
        JarInputStream jis = null;
        try {
            jis = new JarInputStream(new FileInputStream(jar));
            Manifest bundleManifest = jis.getManifest();
            
            return bundleManifest;
        } catch (Exception e) {
            throw new RuntimeException(e)
        } finally {
            try {
                jis.close();
            } catch (IOException e) {
                throw new RuntimeException(e)
            }
        }
    }

    @Override
    public void setRepositoryId(String id) {
        m_id = id;
    }

    @Override
    public String getRepositoryId() {
        return m_id;
    }

    @Override
    public void setUrl(String url) {
        m_dir = url;
    }

    @Override
    public String getUrl() {
        return m_dir;
    }

    @Override
    public void clearCache() {
    }
}
