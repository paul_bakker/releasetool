package org.amdatu.releasetool.api;

public class Artifact {
    private final String m_name;
    private final String m_version;

    public Artifact(String name, String version) {
        super();
        m_name = name;
        m_version = version;
    }

    public String getName() {
        return m_name;
    }

    public String getVersion() {
        return m_version;
    }

    @Override
    public String toString() {
        return m_name + " - " + m_version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((m_name == null) ? 0 : m_name.hashCode());
        result = prime * result + ((m_version == null) ? 0 : m_version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Artifact other = (Artifact) obj;
        if (m_name == null) {
            if (other.m_name != null)
                return false;
        }
        else if (!m_name.equals(other.m_name))
            return false;
        if (m_version == null) {
            if (other.m_version != null)
                return false;
        }
        else if (!m_version.equals(other.m_version))
            return false;
        return true;
    }

    
}
